<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Getting or offering mentoring support</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Noto+Sans:400,400i,700,700i&amp;subset=cyrillic,latin-ext" rel="stylesheet">
    <link type="text/css" rel="stylesheet" href="https://www.bath.ac.uk/lens/106/css/university-of-bath/university-of-bath.css">
    <link rel="apple-touch-icon" sizes="152x152" href="https://www.bath.ac.uk/lens/106/assets/university-of-bath/images/favicon/apple/apple-touch-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="https://www.bath.ac.uk/lens/106/assets/university-of-bath/images/favicon/apple/apple-touch-icon-180x180.png">
    <link rel="icon" sizes="192x192" type="image/png" href="https://www.bath.ac.uk/lens/106/assets/university-of-bath/images/favicon/android/android-chrome-192x192.png">
    <link rel="manifest" href="https://www.bath.ac.uk/lens/106/assets/university-of-bath/images/favicon/manifest.json">
    <link rel="shortcut icon" type="image/x-icon" href="https://www.bath.ac.uk/lens/106/assets/university-of-bath/images/favicon/favicon.ico">
    <meta name="msapplication-config" content="https://www.bath.ac.uk/lens/106/assets/university-of-bath/images/favicon/browserconfig.xml">
    <meta name="msapplication-TileImage" content="https://www.bath.ac.uk/lens/106/assets/university-of-bath/images/favicon/windows/mstile-144x144.png">
    <meta name="msapplication-TileColor" content="#004489">
  </head>
  <body class="Guide">
    <header class="university-header">
    <div class="row column">
      <div class="university-header-flex">
        <div class="brand">
          <a href="/" class="logo">University of Bath</a>
        </div>
        <nav class="role-nav">
          <ul aria-label="Role navigation">
            <li><a href="https://www.bath.ac.uk/staff/">Staff</a></li>
            <li><a href="https://www.bath.ac.uk/students/">Students</a></li>

            <li><a href="https://www.bath.ac.uk/professional-services/development-alumni-relations/">Alumni</a></li>
          </ul>
        </nav>
      </div>
    </div>
  </header>

  <div class="university-nav-bar">
    <div class="row column">
      <div class="university-nav-bar-flex">

        <div class="brand">
          <a href="/" class="logo">University of Bath</a>
        </div>
        <nav class="university-nav">
          <input class="university-nav-checkbox" id="university-nav-checkbox" type="checkbox" aria-labelledby="university-nav-label">
          <label for="university-nav-checkbox" class="university-nav-label" id="university-nav-label" aria-haspopup="true">Open menu</label>
          <div class="university-nav-tray">
            <ul aria-label="Site navigation">
              <li><a href="https://www.bath.ac.uk/courses/">Courses</a></li>
              <li><a href="https://www.bath.ac.uk/topics/research/">Research</a></li>
              <li><a href="https://www.bath.ac.uk/topics/collaborate-with-us/">Collaborate</a></li>
              <li><a href="https://www.bath.ac.uk/departments/">Departments</a></li>
              <li><a href="https://www.bath.ac.uk/topics/about-the-university/">About</a></li>
            </ul>
            <nav class="role-nav">
              <ul aria-label="Role navigation">
                <li><a href="https://www.bath.ac.uk/staff/">Staff</a></li>
                <li><a href="https://www.bath.ac.uk/students/">Students</a></li>

                <li><a href="https://www.bath.ac.uk/professional-services/development-alumni-relations/">Alumni</a></li>
              </ul>
            </nav>
          </div>
        </nav>
        <div class="university-search">
          <input class="university-search-checkbox" id="university-search-checkbox" type="checkbox" aria-labelledby="university-search-label">
          <label for="university-search-checkbox" class="university-search-label" id="university-search-label">Open search</label>
          <div class="university-search-tray">
            <form class="university-search-form" method="get" action="https://www.bath.ac.uk/search">
              <input class="search-field" name="query" placeholder="Search bath.ac.uk" title="Type search term here" type="text">
              <input name="collection" value="website" type="hidden">
              <input class="search-button" value="Search" type="submit" aria-label="Search bath.ac.uk">
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>

  <main>
    <div class="medium-padding stack-section">
      <div class="row column">
        <div class="flex-wrapper">
          <div class="single-item">
            <h1 class="page-heading">Getting or offering mentoring support</h1>
            <h2 class="page-subheading">An overview of mentoring and how to request one-to-one mentoring support from a more experienced colleague in the Education and Research job family.</h2>
          </div>
        </div>
      </div>
    </div>

    <section class="markdown bg-steel-85-light small-padding">
      <div class="row column">
        <div class="flex-wrapper">
          <article class="single-item">
            <div class="sidebar pull-right">
            <!-- On this page -->
              <nav class="panel in-page-nav bg-mint-75-light">
                <h1>On this page</h1>
                <ul>
                  <li><a href="#mentoring">Mentoring</a></li>
                  <li><a href="#mentoring-scheme-for-staff-in-Education-research-job-family">Mentoring scheme for staff in Education & Research job family</a></li>
                  <li><a href="#resources">Resources</a></li>
                </ul>
              </nav>
            </div>
            <!-- narrative block items -->
            <h1 id="mentoring">Mentoring</h1>
            <p>Mentoring is a form of one-to-one support where a more experienced colleague uses their knowledge, skills and connections to help someone with their current and future challenges.  It has been shown to have a positive effect on individuals’ career success and organisational return on investment.</p>
            <p>A mentor is often described as a “critical friend” or “supportive challenger,” because they have a role in helping people become more self-aware and to take responsibility for solving their own problems. However, mentors are distinct from most coaches in that they have often walked the same path before, and use their own experience to support the mentee. They may be able to help with general professional and career advice, or may have particular expertise that is relevant to the mentee’s situation. For example, they may be very successful at engaging industry in research, or at managing people, while the mentee is only starting to develop in those areas.</p>
            <p>Although the University does not have a formal mentoring system for all staff, <a href="/publications/appendix-9-of-cper-role-of-mentors/">academic staff are allocated a mentor as part of their probation support.</a></p>
            <p>All staff in the Education and Research job family can request a mentor through their departmental mentoring coordinator. </p>
            <p>Members of staff are free to seek their own mentors, and it is expected that where appropriate and where workloads permit, experienced staff should be prepared to support colleagues in this way.  If you are having difficulty identifying a potential mentor or do not feel able to approach one, you should seek help from your manager or more senior managers in your department.</p>
            <h1 id="mentoring-scheme-for-staff-in-education-research-job-family"></h1>
            <p>This scheme is founded on the principles that experienced academic colleagues are best placed to help staff identify and connect with a suitable mentor, and that supporting less experienced colleagues by mentoring is a fundamental feature of an academic role. Organisation of the process is therefore rooted in the academic community, with a light administrative oversight and support for co-ordinators and mentors to develop the necessary skills and networks.</p>
            <h2>Definitions</h2>
            <p>Mentors in this context provide informal support for colleagues in their career and welfare. It is distinct from the mentors assigned to probationary lecturers who have a specific role. Mentoring is best performed by someone who has trodden the path of the mentee before them, whether this refers to the whole scope of an academic career, broader experience outside the University, or a specific current concern. </p>
            <p>The Education and Research Job Family includes lecturers, senior lecturers, readers, professors, research assistants, research fellows and teaching fellows.</p>
            <h2>Eligibility</h2>
            <p>The scheme is open to all Education and Research Staff who have completed probation. It is a voluntary, developmental scheme unconnected to career progression.&nbsp;</p>
            <h2>Process: finding a mentor or offering to be a mentor</h2>
            <p>Any member of Education and Research staff (academics, researchers and teaching fellows) who wishes to find a mentor, or who wishes to offer their services as a mentor, should contact their departmental / School mentoring co-ordinator in the first instance</p>
            <h2>Departmental mentoring co-ordinators</h2>
            <ul>
              <li>Architecture &amp;&nbsp;Civil Engineering: Dr Alexander Copping</li>
              <li>Biology &amp;&nbsp;Biochemistry:&nbsp;Dr Hazel Corradi</li>
              <li>Chemical Engineering:&nbsp;Prof Semali Perera</li>
              <li>Chemistry:&nbsp;Prof Jon Williams</li>
              <li>Computer Science:&nbsp;Prof Guy McCusker</li>
              <li>Economics: Dr Peter Postl</li>
              <li>Education: Prof Chris James</li>
              <li>Electronic &amp;&nbsp;Electrical Engineering:&nbsp;Prof Cathryn Mitchell</li>
              <li>Health:&nbsp;Dr Fiona Gillison</li>
              <li>Management:&nbsp;Prof Juani Swart</li>
              <li>Mathematical Sciences: Dr Lucia Scardia</li>
              <li>Mechanical Engineering: Prof Richie Gill and Dr Marcelle McManus</li>
              <li>Pharmacy &amp;&nbsp;Pharmacology:&nbsp;Prof Mark Lindsay</li>
              <li>Physics:&nbsp;Dr Daniel Wolverson</li>
              <li>Politics, Languages &amp;&nbsp;International Studies: Mr Brett Edwards</li>
              <li>Psychology:&nbsp;Dr Catherine Hamilton-Giachritsis and Dr&nbsp;Anthony Little</li>
              <li>Social &amp;&nbsp;Policy Sciences:&nbsp;Dr Kate Woodthorpe</li>
            </ul>
            <h1 id="resources">Resources</h1>
            <ul>
              <li><a href="http://www.bath.ac.uk/hr/hrdocuments/learning/e-and-r-mentoring-scheme.pdf">Read the details of the scheme</a></li>
              <li><a href="http://www.bath.ac.uk/hr/hrdocuments/learning/e-and-r-mentoring-process.pdf">See a flowchart showing&nbsp;the mentor-matching process</a></li>
              <li><a href="http://www.bath.ac.uk/hr/hrdocuments/learning/mentoring-guidelines.pdf">Read general tips and guidelines about mentoring and being mentored</a></li>
              <li><a href="/guides/learn-online-using-the-development-toolkit/">View guidance and tips for mentoring on the Development Toolkit</a></li>
            </ul>
          </article>
        </div>
      </div>
    </section>

    <div class="about-contact bg-cornflower-25-light small-padding">
      <section class="contact-us small-padding text-only stack-section">
        <header class="text-center">
            <div class="row column">
                <div class="section-header">
                    <h1 class="section-heading">Enquiries</h1>
                    <p>If you have any questions, please contact us </p>
                </div>
                <hr class="section-header-divider" />
            </div>
        </header>
        <div class="row column" data-equalizer="card-header" data-equalize-on="medium">
          <div class="flex-wrapper small-padding">
            <article class="card standard-card">
              <header data-equalizer-watch="card-header">
                  <h1>Simon Inger, Staff Development Unit</h1>
              </header>
              <footer>
                <ul class="no-bullet-list">
                  <li class="email"><a href="mailto:s.inger@bath.ac.uk">s.inger@bath.ac.uk</a></li>
                  <li class="telephone">01225 383890</li>
                </ul>
              </footer>
            </article>
          </div>
        </div>
      </section>
    </div>
  </main>

  <div class="university-awards bg-granite-10-light small-padding">
    <div class="row column text-center">
      <div class="flex-wrapper">
        <div class="award-item">
          <a href="https://www.bath.ac.uk/topics/sports-research/">
            <img src="https://www.bath.ac.uk/lens/106/assets/university-of-bath/images/awards/guardian-top-10-uk-2020.svg" alt="Top 10 UK University, The Guardian 2020">
          </a>
        </div>
        <div class="award-item">
          <img src="https://www.bath.ac.uk/lens/106/assets/university-of-bath/images/awards/tef-ranking.svg" alt="Teaching Excellence Framework Gold">
        </div>
        <div class="award-item">
          <a href="https://www.bath.ac.uk/corporate-information/rankings-and-reputation/">
            <img src="https://www.bath.ac.uk/lens/106/assets/university-of-bath/images/awards/times-graduate-prospects-2019.svg" alt="Top 5 for graduate prospects 2019, The Times and The Sunday Times">
          </a>
        </div>
      </div>
    </div>
  </div>

  <footer class="university-footer">
    <div class="row">
      <div class="footer-wrapper">
        <div class="column medium-12">
          <a class="copyright" href="https://www.bath.ac.uk/corporate-information/copyright/">&copy; University of Bath 2019</a>
          <ul class="reverse no-bullet-list">
            <li><a href="https://www.bath.ac.uk/corporate-information/disclaimer/">Disclaimer</a></li>
            <li><a href="https://www.bath.ac.uk/foi/">Freedom of information</a></li>
            <li><a href="https://www.bath.ac.uk/corporate-information/privacy-and-cookie-policy/">Privacy and cookie policy</a></li>
            <li><a href="http://www.bath.ac.uk/index">A&ndash;Z</a></li>
          </ul>
        </div>
        <div class="column medium-12">
          <div class="suggest-improvement">
            <a href="#" id="suggest-improvement-link">Suggest an improvement</a>
          </div>
        </div>
      </div>
    </div>
  </footer>

  <script>
    (function() {

      var formParameters = '?entry_247245321=' + encodeURIComponent(document.location);
      var feedbackLink = document.getElementById("suggest-improvement-link");
      feedbackLink.href = feedbackLink.href + formParameters;
    })();
  </script>
  <script src="https://www.bath.ac.uk/lens/106/assets/university-of-bath/js/vendor/jquery-3.2.1.min.js"></script>
  <script src="https://www.bath.ac.uk/lens/106/assets/university-of-bath/js/foundation.min.js"></script>
  <script>
    $(window).on('load', function () {
      $(document).foundation();
    });
  </script>
  </body>
</html>

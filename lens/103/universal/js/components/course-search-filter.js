/* global $ */

/**************************************************************************************************************
 *
 * Allow users to select a compound course search query from a single drop-down menu.
 *
 * USAGE:
 * Replace the div with the id "search-options-no-js" containing two select menus:
 *
 *  <div class="search-options" id="search-options-no-js">
 *    <label class="screen-reader" for="course-type-filter" id="filter-label">Filter your search</label>
 *    <select name="f.Type of course|C" class="course-type-filter" id="course-type-filter" aria-labelledby="filter-label">
 *      <option value="" selected>All courses</option>
 *      <option value="undergraduate" >Undergraduate courses</option>
 *      <option value="postgraduate" >Taught postgraduate courses</option>
 *      <option value="postgraduate research" >Postgraduate research courses</option>
 *    </select>
 *    <select name="f.Year of entry|E" class="entry-year-filter" id="entry-year-filter" aria-labelledby="filter-label">
 *      <option value="">Year</option>
 *      <option value="2018" >2018</option>
 *      <option value="2019" >2019</option>
 *    </select>
 *   </div>
 *
 * with a single select menu and two hidden fields:
 *
 *  <div class="search-options" id="search-options-active-js">
 *    <label id="filter-label class="screen-reader" for="course-type-filter">Filter your search</label>
 *    <select id="course-type-filter">
 *      <option value="">All courses</option>
 *      <option value="undergraduate|2018">Undergraduate courses 2018</option>
 *      <option value="undergraduate|2019">Undergraduate courses 2019</option>
 *      <option value="postgraduate">Taught postgraduate courses</option>
 *      <option value="postgraduate research">Postgraduate research courses</option>
 *    </select>
 *    <input type="hidden" name="f.Type of course|C" value="">
 *    <input type="hidden" name="f.Year of entry|E" value="">
 *  </div>
 *
 * Populate the hidden fields with the value of the selection before submitting the form.
 *
 * The options are closely coupled with the replacement.
 **************************************************************************************************************/
/***
 * Get current div and extract preselected options
 */

var courseTypeSelected = $('#course-type-filter option').filter(':selected').val()
var courseYearSelected = $('#entry-year-filter option').filter(':selected').val()

/***
 * Create new div, preselecting appropriate option
 */

var selectedItem = ''

if (courseTypeSelected !== '' && courseYearSelected !== '') {
  selectedItem = courseTypeSelected + '|' + courseYearSelected
} else if (courseTypeSelected !== '') {
  selectedItem = courseTypeSelected
} else if (courseYearSelected !== '') {
  selectedItem = courseYearSelected
}

var newDiv = $('<div/>', { id: 'search-options-active-js', class: 'search-options' })
newDiv.append($('<label/>', { id: 'filter-label', class: 'screen-reader', for: 'course-search-filter' }).append('Filter your search'))
var newSection = $('<select/>', { id: 'course-search-filter', 'aria-labelledby': 'filter-label' })
newSection.append($('<option/>', { value: '' }).append('All courses'))
newSection.append($('<option/>', { value: 'undergraduate|2019', selected: (selectedItem === 'undergraduate|2019') }).append('Undergraduate &ndash; 2019 courses'))
newSection.append($('<option/>', { value: 'postgraduate|2019', selected: (selectedItem === 'postgraduate|2019') }).append('Taught postgraduate &ndash; 2019 courses'))
newSection.append($('<option/>', { value: 'postgraduate research|2018', selected: (selectedItem === 'postgraduate research|2018') }).append('Postgraduate research &ndash; 2018 courses'))
newSection.append($('<option/>', { value: 'postgraduate research|2019', selected: (selectedItem === 'postgraduate research|2019') }).append('Postgraduate research &ndash; 2019 courses'))
newDiv.append(newSection)
newDiv.append($('<input/>', { type: 'hidden',
                              value: courseTypeSelected,
                              name: 'f.Type of course|C',
                              id: 'type-of-course' }))
newDiv.append($('<input/>', { type: 'hidden',
                              value: courseYearSelected,
                              name: 'f.Year of entry|E',
                              id: 'year-of-entry' }))

/***
 * Replace old div with new div
 */

$('#search-options-no-js').replaceWith(newDiv)

/***
 * Set the values of the hidden field before submitting the form
 */
$('form').submit(function (e) {
  var filters = $('#course-search-filter option').filter(':selected').val().split('|')
  var typeFilter = filters[0]
  var yearFilter = filters[1]

  $('#type-of-course').val(typeFilter)
  $('#year-of-entry').val(yearFilter)
})

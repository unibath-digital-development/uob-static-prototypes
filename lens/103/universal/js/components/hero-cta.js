/**************************************************************************************************************
 *
 * A CTA button at the top right of the hero image on Course pages
 * (could also work on other Lens pages with heroes, but untested)
 *
 * Should be triggered in Google Tag Manager.
 *
 * USAGE:
 * Include two script elements in a 'custom HTML' GTM tag:
 *
 * <script src='http://www.bath.ac.uk/lens/universal/js/components/hero-cta.js'></script>
 * <script>
 *   createHeroCta({ text: 'your button text',                       // A string between 2 and 45 characters
 *                   link: 'http://your.link/here',                  // A valid URL
 *                   tracking_class: 'unique-class-for-analytics' }) // Optional class for click tracking
 * </script>
 *
 **************************************************************************************************************/

/***
 * Validates the length of the button text to avoid regression.
 */
function invalid (params) {
  var valid = true
  if (params.text.length < 2 || params.text.length > 45) { (valid = false) }
  return !valid
}

/***
 * The function to be called from GTM.
 *
 * Inserts an `a.header-cta-button` element after the UoB logo in the hero image.
 */
function createHeroCta (params) {
  if (invalid(params)) {
    console.log('Button text is invalid')
    return
  }

  // Constants for insertion targeting
  var headerInner = document.getElementsByClassName('header-inner')[0]
  var subHeader = document.getElementsByClassName('sub-header')[0]

  // Create the CTA element
  var cta = document.createElement('a')
  cta.href = params.link
  cta.innerText = params.text
  // IE doesn't support multiple args in the .classList.add() method
  cta.classList.add('header-cta-button')
  cta.classList.add('header-cta')
  // Add tracking parameters if specified in GTM
  if (params.tracking_class) { cta.classList.add(params.tracking_class) }

  // Show the sub-header container on mobile
  subHeader.classList.remove('hide-for-small-only')
  // Give the sub-header container a granite (dark) background
  subHeader.classList.add('bg-granite-10-light')

  // Insert the CTA inside the .header-inner element
  headerInner.appendChild(cta)
}

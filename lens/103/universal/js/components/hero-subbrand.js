/**************************************************************************************************************
 *
 * A sub brand logo at the top right of the hero image
 *
 * Should be triggered in Google Tag Manager.
 *
 * USAGE:
 * Include two script elements in a 'custom HTML' GTM tag:
 *
 * <script src='http://www.bath.ac.uk/lens/universal/js/components/hero-subbrand.js'></script>
 * <script>
 *   createSubbrand({ data: '<g fill.../>',        // A string containing the innerhtml for the svg tag
                      width: 220,                  // Width of the element
                      height: 70,                  // Height of the element
                      class: 'header-subbrand' })  // The class of the svg element
 * </script>
 *
 **************************************************************************************************************/

/***
 * The function to be called from GTM.
 *
 * Inserts an `img` element after the UoB logo in the hero image.
 */
function createSubbrand (params) {
  // Constants for insertion targeting
  var headerInner = document.getElementsByClassName('header-inner')[0]

  // Create the sub brand element
  var subBrand = document.createElementNS('http://www.w3.org/2000/svg', 'svg')
  subBrand.setAttribute('xmlns', 'http://www.w3.org/2000/svg')
  subBrand.setAttribute('width', params.width)
  subBrand.setAttribute('height', params.height)
  subBrand.setAttribute('class', params.class)
  subBrand.innerHTML = params.data

  // Show the sub-header container on mobile
  document.getElementsByClassName('sub-header')[0].classList.remove('hide-for-small-only')
  // Give the sub-header container a granite (dark) background
  document.getElementsByClassName('sub-header')[0].classList.add('bg-granite-10-light')

  // Insert the sub brand logo inside the .header-inner element
  headerInner.appendChild(subBrand)
}

/**************************************************************************************************************
 *
 * A pop-up sticky footer for use on Lens pages
 *
 * Should be triggered in Google Tag Manager.
 *
 * USAGE:
 * Include three script elements in a 'custom HTML' GTM tag:
 *
 * <script src='http://www.bath.ac.uk/lens/universal/js/vendor/jquery.waypoints.min.js'></script>
 * <script src='http://www.bath.ac.uk/lens/universal/js/components/page-bottom-sticky-stripe.js'></script>
 * <script>
 *   setPopUp({
 *     text:        [YOUR BANNER TEXT], // required; must be between 10 and 160 characters
 *     button_text: [YOUR BUTTON TEXT], // required; must be between 2 and 45 characters
 *     button_link: [YOUR BUTTON LINK]  // required; must be longer than 2 characters
 *   });
 * </script>
 *
 **************************************************************************************************************/

/***
 * Set waypoint for triggering pop-up appearance/disappearance.
 *
 * Requires /universal/js/vendor/jquery.waypoints.min.js
 */
var waypoint = new Waypoint({
  element: $('.hero'),
  handler: setPositionOfPopUpWhenPageScrolls,
  offset: function() { return -$('.hero').height(); }
});

/***
 * Returns the sum of the height, and top and bottom padding of the element matching the given
 * selector.
 */
function heightAndPaddingOf(selector) {
  return $(selector).innerHeight();
}

/***
 * Sets the `bottom` attribute on an element matching `#pop-up` to either the negative of its height,
 * or to 0.
 *
 * The scroll effect requires that the element be `position: fixed` and ideally has some `transition` value
 * set.
 */
function setPositionOfPopUpWhenPageScrolls(direction) {
  var bottom_position = direction == 'up' ? -heightAndPaddingOf('#pop-up') : 0;
  $('#pop-up').css('bottom', bottom_position);
}

/***
 * Ensures footer remains visible in its entirety by boosting it up
 * by the height of an element matching the given selector.
 *
 * Useful for handling bottom-sticky popups...
 */
function increaseBottomPaddingOfFooterByHeightOf(selector) {
  var footer = $('body > footer');
  var popup_height = heightAndPaddingOf(selector);
  var modified_padding = parseInt($(footer).css('padding-bottom')) + popup_height;
  $(footer).css('padding-bottom', modified_padding);
}

/***
 * Removes the contact section DOM element, and inserts a popup with the given values
 */
function replaceContactSectionWithPopUp(text, button_text, button_link) {
  var pop_up = '<section id="pop-up" class="pop-up bg-cornflower-60-dark small-padding">\
                  <div class="row column">\
                    <div class="flex-wrapper">\
                      <div class="single-item pop-up-inner">\
                        <div class="pull-right pop-up-pull-element">\
                          <a href="' + button_link + '" class="button pop-up-button v-center">' + button_text + '</a>\
                        </div>\
                        <div class="hide-for-small-only">\
                          <div class="wordmark-cornflower">\
                            University of Bath\
                          </div>\
                          <h1 class="pop-up-heading">' + text + '</h1>\
                        </div>\
                      </div>\
                    </div>\
                  </div>\
                </section>';
  $('.about-contact').replaceWith(pop_up);
  increaseBottomPaddingOfFooterByHeightOf('#pop-up');
}

/***
 * Validates the parameters for the banner.
 * If you are passing in HTML mark up (like a <span>),
 * it will include the characters that comprise the tags in the limit.
 *
 * If the text parameter exceeds 3 lines within the banner, be aware that the
 * styles break and you will have to fix them. Just an FYI.
 */
function invalid(params) {
  var valid = true;

  var text = params.text;
  var button_text = params.button_text;
  var button_link = params.button_link;

  if (text.length < 10 || text.length > 160) { valid = false }
  if (button_text.length < 2 || button_text.length > 45) { valid = false }
  if (button_link.length < 2) { valid = false }

  return !valid;
}

/***
 * This is the function to be called within GTM. It creates a pop-up
 * that is triggered by a waypoint on `#hero`.
 *
 * Required parameters: { text:        [STRING BETWEEN 10 AND 160 CHARS],
 *                        button_text: [STRING BETWEEN 2 AND 45 CHARS],
 *                        button_link: [URL GREATER THAN 2 CHARS] }
 */
function setPopUp(params) {
  if (invalid(params)) {
    console.log('Pop up texts or URL are not valid');
    return;
  }
  replaceContactSectionWithPopUp(params.text, params.button_text, params.button_link);
}


